## Getting started

   sudo apt-get update && sudo apt-get upgrate -y

   sudo apt install wget git curl snapd zsh -y

## Chrome

   wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

   sudo dpkg -i google-chrome-stable_current_amd64.deb

## Ohmyzsh

   sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

## Docker

   sudo apt update
   
   sudo apt install apt-transport-https ca-certificates curl software-properties-common -y

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

   sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

   sudo apt update
   
   sudo apt install docker-ce -y
   
   sudo usermod -aG docker ${USER}
   
   su - ${USER}

## Docker Compose

   sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

   sudo chmod +x /usr/local/bin/docker-compose

## Install php storm and web storm with snapd

   sudo snap install phpstorm --classic
   
   sudo snap install webstorm --classic

## Install Visual Studio Code with snapd

   sudo snap install --classic code
   
## Install ibus bamboo

   sudo add-apt-repository ppa:bamboo-engine/ibus-bamboo
    
   sudo apt-get update
    
   sudo apt-get install ibus-bamboo
    
   ibus restart

